var express = require('express');
const bodyParser = require('body-parser');
var app = express();
const Post = require('./models/posts');
const path = require('path');

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/javascript', express.static(path.join(__dirname, 'node_modules', 'jquery', 'dist')));
//const arr = ["hello", "world", "test"];

app.get('/', function(req, res) {
	res.render('index');
	//res.render("create");
});

module.exports = app;
