const app = require('./app');
const database = require('./database');
const conf = require('./config');

database()
	.then((info) => {
		console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
		app.listen(conf.PORT, function() {
			console.log(`Example app listening on port ${conf.PORT}!`);
		});
	})
	.catch(() => {
		console.error('Unable to connect to database');
		process.exit(1);
	});
